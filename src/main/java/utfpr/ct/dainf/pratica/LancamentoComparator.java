package utfpr.ct.dainf.pratica;
import java.util.Comparator;

/**
 * Linguagem Java
 * @author a1937553
 */
public class LancamentoComparator implements Comparator<Lancamento> {
    public int compare(Lancamento p1, Lancamento p2) {
        if(p1.getConta().compareTo(p2.getConta())!=0){
            return p1.getConta().compareTo(p2.getConta());
        }
        return p1.getData().compareTo(p2.getData());
    }
}
